function start() {
    let x = $("#dateUntil").val();

    const finishDate = new Date(x);
    const todayDate = new Date();
    const eventName = $("#eventName").val();

    $("#timer-title").text("Time until " + eventName + " :");

    let diffTime = Math.abs(finishDate - todayDate);
    diffTime = Math.ceil(diffTime / 1000);
    convertTime(diffTime);

    const interval = setInterval(function() {
        const todayDate = new Date();
        let diffTime = Math.abs(finishDate - todayDate);
        diffTime = Math.ceil(diffTime / 1000);
        convertTime(diffTime);
    }, 1000);
}

function convertTime(n) {
    var day =parseInt( n / (24 * 3600));

    n = n % (24 * 3600);
    var hour = parseInt(n / 3600);

    n %= 3600;
    var minutes = n / 60;

    n %= 60;
    var seconds = n;

    writeResults(day, hour, minutes.toFixed(), seconds.toFixed());
}

function writeResults (d, h, m, s) {
    $("#timer").text(d + " days, " + h + ":" + m + ":" + s);
}